
# Change Log

The format of this changelog is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 2.0 (unreleased)

The next major verion of Depo may be a significant departure in how it is used. 

There will be a lower-level SQLite NIF library with a simple interface to SQLite C API functions. 

Depo will be built on top of that, and will be a web service for managing many data services using SQLite underneath, and an HTTPS API and a graphical web interface. All data services require authentication, which is managed with roles by an authentication service. A role specifies read/write access for specific data services identified by their URL. The tokens must be retrieved from the authentication service, and included in requests to the individual data services. 

Each data service is specified by a group of SQL functions. An `init` function is used for creating the initial database schema, and `read` and `write` functions are for querying and updating the database. Each `read` and `write` function becomes a `GET` or `POST` endpoint, and cannot be changed after creation. Only new endpoints can be added. To make backwards-incompatible changes, you must create a new data service. 

You can generate a token in SQLite with `select lower(hex(randomblob(16)));`, and get the current time with `select strftime('%Y-%m-%dT%H:%M:%f');`. 

## 1.7.1

### Updated
- updated Elixir and ExDoc dependencies to latest versions

## 1.7

### Removed
- remove `:single` and `:values` options to `teach` to simplify usage

## 1.6

### Added
- added `read_stream/1` to offer better handling of streams

### Changed
- made `transact/2` return value from last expression within transaction function (fixes issue #29)

### Fixed
- fixed broken tests that were using `Process.info/1` to read streams

## 1.5.2

### Fixed
- fixed bug where `:single` commands would return improperly formatted values when given bound variables

## 1.5.1

### Fixed
- updated links in package info

## 1.5.0

### Added
- human readable changelog in README (fixes issue #20)
- improved `Depo.DB` formatting in logs
- internal refactor to share implementation between async and sync queries
- options for returning query values as lists of tuples, maps, or a single value (fixes issue #26)
- option for sending a single value to bind to a query not in a list (fixes issue #24)
- updated documentation to include information on why and how to use `teach/2` and `transact/2`
