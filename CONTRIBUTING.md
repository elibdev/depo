
# Contributing

Thank you for your interest!

Please feel free to make a new issue for any questions, 
bugs, or feature ideas. 
Pull requests are welcome, but please make an issue first
for large changes so we can discuss the best design before 
you spend your time writing all the code and tests first. 

## Pre-Release Checklist

1. `mix test`
1. change version number in `mix.exs` to vX.Y.Z in accordance with [semantic versioning](http://semver.org/)
1. update the version number in the `README.md` installation section
1. change the "Unreleased" heading in the `CHANGELOG.md` to vX.Y.Z
1. `git add -A .`
1. `git commit -m "updated version number to X.Y.Z"`
1. `git tag vX.Y.Z`
1. `git push`
1. `git push origin vX.Y.Z`
1. `mix hex.publish`

