defmodule Depo.Mixfile do
  use Mix.Project

  def project do
    [
      app: :depo,
      version: "1.7.1",
      elixir: "~> 1.7",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "Depo",
      source_url: "https://gitlab.com/elibdev/depo/",
      docs: [
        main: "readme",
        extras: [
          "README.md": [title: "Overview"],
          "CHANGELOG.md": [title: "Change Log"]
        ]
      ]
    ]
  end

  defp description do
    """
    Depo provides lightweight storage and querying capabilities
    in Elixir by providing a minimal and polished API
    that builds on the unique advantages of SQLite.
    """
  end

  defp package do
    [
      name: :depo,
      files: ["lib", "mix.exs", "README*", "LICENSE*", "CHANGELOG*"],
      maintainers: ["Eli Bierman"],
      licenses: ["MIT"],
      links: %{
        "Source & Issues @ GitLab" => "https://gitlab.com/elibdev/depo/"
      }
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:esqlite, "~> 0.2.3"}
    ]
  end

  def application do
    []
  end
end
