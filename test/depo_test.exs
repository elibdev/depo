defmodule DepoTest do
  @moduledoc """
  Each test should have a corresponding Github issue:

  https://github.com/sunsetworks/depo/issues

  The name of each test follows a specific format:

  test "<issue-id>.<test-id> - optional brief description" do
  ...end

  <issue> is the number of the relevant Github issue,
  and together with <test-id> uniquely identifies a test.

  You can find a specific issue at:
  https://github.com/sunsetworks/depo/issues/<issue-id>
  """
  use ExUnit.Case
  doctest Depo

  setup do
    # Remove any database around from old tests.
    path = Path.join(System.tmp_dir(), "depo.db")
    if File.exists?(path) do
      File.rm(path)
    end
    :ok
  end

  test "1.1 - open and close in-memory db" do
    {:ok, db} = Depo.open(:memory)
    assert :ok == Depo.close(db)
  end

  test "9.1 - create and close disk-backed db" do
    path = Path.join(System.tmp_dir(), "depo.db")
    assert(not(File.exists?(path)), 
      "Database file shouldn't exist first.")
    {:ok, db} = Depo.open(create: path)
    assert(File.exists?(path),
      "Database should exist after being created.")
    assert :ok == Depo.close(db)
    File.rm(path)
  end

  test "9.2 - create, close then open disk-backed db" do
    path = Path.join(System.tmp_dir(), "depo.db")
    assert(not(File.exists?(path)), 
      "Database file shouldn't exist first.")
    {:ok, db} = Depo.open(create: path)
    Depo.write(db, "CREATE TABLE words (word)")
    Depo.write(db, "INSERT INTO words VALUES ('ok')")
    assert(File.exists?(path),
      "Database should exist after being created.")
    assert :ok == Depo.close(db)

    {:ok, db} = Depo.open(path)
    assert Depo.read(db, "SELECT * FROM words") == [
      %{word: "ok"}]
    assert :ok == Depo.close(db)
    File.rm(path)
  end

  test "9.3 - creating db at existing path fails" do
    Process.flag(:trap_exit, true)
    path = Path.join(System.tmp_dir(), "depo.db")
    assert(not(File.exists?(path)), 
      "Database file shouldn't exist first.")
    {:ok, db} = Depo.open(create: path)
    assert :ok == Depo.close(db)
    assert Depo.open(create: path) == {:error, :file_already_exists}
    File.rm(path)
  end

  test "9.4 - create and correctly use disk db" do
    path = Path.join(System.tmp_dir(), "depo.db")
    {:ok, db} = Depo.open(create: path)
    Depo.write(db, "CREATE TABLE greetings (phrase)")
    Depo.teach(db, %{
      new_greeting: "INSERT INTO greetings VALUES (?1)",
      greetings: "SELECT * FROM greetings",
    })
    Depo.transact(db, fn ->
      Enum.each(["hola", "bonjour", "今日は"], fn phrase ->
        Depo.write(db, :new_greeting, [phrase])
      end)
    end)
    stream_id = Depo.stream(db, self(), :greetings)
    messages = Depo.read_stream(stream_id)
    assert messages == [
      %{phrase: "今日は"},
      %{phrase: "bonjour"},
      %{phrase: "hola"}, 
    ]
    assert :ok == Depo.close(db)
    File.rm(path)
  end

  test "11.1 - basic read and write to in-memory db" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, """
    CREATE TABLE pets (animal, name);
    INSERT INTO pets VALUES ('dog', 'bubbles');
    INSERT INTO pets VALUES ('cat', 'monet');
    """)
    assert Depo.read(db, "SELECT * FROM pets") == [
      %{animal: "cat", name: "monet"},
      %{animal: "dog", name: "bubbles"},
    ]
    assert :ok == Depo.close(db)
  end

  test "11.2 - multiple writes to memory db" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "CREATE TABLE pets (animal, name)")
    Depo.write(db, "INSERT INTO pets VALUES ('dog', 'bubbles')")
    Depo.write(db, "INSERT INTO pets VALUES ('cat', 'monet')")
    assert Depo.read(db, "SELECT * FROM pets") == [
      %{animal: "cat", name: "monet"},
      %{animal: "dog", name: "bubbles"},
    ]
  end

  test "11.3 - write with bound numbered variables" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "CREATE TABLE parties (date, location, host)")
    Depo.write(db, "INSERT INTO parties VALUES (?1, ?2, ?3)", 
      ["today", "here", "me"])
    Depo.write(db, "INSERT INTO parties VALUES (?1, ?2, ?3)", 
      ["tomorrow", "there", "you"])
    assert Depo.read(db, "SELECT * FROM parties") == [
      %{date: "tomorrow", location: "there", host: "you"},
      %{date: "today", location: "here", host: "me"}, 
    ]
  end

  test "11.4 - teach then write with bound variables" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "
    CREATE TABLE ants (
      id INTEGER PRIMARY KEY,
      type
    ) ")
    Depo.teach(db, %{
      new_ant: "INSERT INTO ants (type) VALUES (?1)",
      all_ants: "SELECT * FROM ants",
    })
    Depo.write(db, :new_ant, ["worker"])
    Depo.write(db, :new_ant, ["worker"])
    assert Depo.read(db, :all_ants) == [
      %{id: 2, type: "worker"}, %{id: 1, type: "worker"}]
  end

  test "11.5 - read with bound numbered variables" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "
    CREATE TABLE friends (
      id INTEGER PRIMARY KEY,
      name
    )")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('joy')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('ash')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('misty')")

    assert Depo.read(db, 
      "SELECT id FROM friends WHERE name=?1", 
      ["misty"]) == [%{id: 3}]
    assert Depo.read(db, 
      "SELECT id FROM friends WHERE name=?1", 
      ["joy"]) == [%{id: 1}]
    assert Depo.read(db, 
      "SELECT id FROM friends WHERE name=?1", 
      ["ash"]) == [%{id: 2}]
  end

  test "11.6 - teach then read with bound variables" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "
    CREATE TABLE friends (
      id INTEGER PRIMARY KEY,
      name
    )")
    Depo.teach(db, %{
      friend_id: "SELECT id FROM friends WHERE name=?1"
    })
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('joy')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('ash')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('misty')")

    assert Depo.read(db, :friend_id,
      ["misty"]) == [%{id: 3}]
    assert Depo.read(db, :friend_id,
      ["joy"]) == [%{id: 1}]
    assert Depo.read(db, :friend_id,
      ["ash"]) == [%{id: 2}]
  end

  test "11.7 - stream with bound variables" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "
    CREATE TABLE friends (
      id INTEGER PRIMARY KEY,
      name
    )")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('joy')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('ash')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('joy')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('joy')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('misty')")

    stream = Depo.stream(db, self(), 
      "SELECT * FROM friends WHERE name=?1", ["joy"])

    messages = Enum.reverse(Depo.read_stream(stream))
    assert messages == [
      %{id: 1, name: "joy"}, 
      %{id: 3, name: "joy"}, 
      %{id: 4, name: "joy"},
    ]
  end

  test "11.8 - teach and stream with bound variables" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "
    CREATE TABLE friends (
      id INTEGER PRIMARY KEY,
      name
    )")
    Depo.teach(db, %{
      new_friend: "INSERT INTO friends (name) VALUES (?1)",
      friends_named: "SELECT * FROM friends WHERE name=?1",
    })
    Depo.write(db, :new_friend, ["joy"])
    Depo.write(db, :new_friend, ["ash"])
    Depo.write(db, :new_friend, ["joy"])
    Depo.write(db, :new_friend, ["joy"])
    Depo.write(db, :new_friend, ["misty"])

    stream = Depo.stream(db, self(), :friends_named, ["joy"])
    messages = Depo.read_stream(stream) |> Enum.reverse()
    assert messages == [
      %{id: 1, name: "joy"}, 
      %{id: 3, name: "joy"}, 
      %{id: 4, name: "joy"},
    ]
  end

  test "12.1 - stream simple query from db" do
    {:ok, db} = Depo.open(:memory)
    phrases = ["hola", "bonjour", "こんいちは"]
    Depo.transact(db, fn ->
      Depo.write(db, "CREATE TABLE greetings (phrase);")
      Enum.each(phrases, fn phrase ->
        Depo.write(db, 
          "INSERT INTO greetings VALUES (?1)", [phrase])
      end)
    end)
    stream_1 = Depo.stream(db, self(), "SELECT * FROM greetings")
    stream_2 = Depo.stream(db, self(), "SELECT * FROM greetings")
    assert(stream_1 != stream_2, "stream IDs should be unique")
    messages_1 = Depo.read_stream(stream_1) |> Enum.reverse()
    messages_2 = Depo.read_stream(stream_2) |> Enum.reverse()
    messages = [
      %{phrase: "hola"},
      %{phrase: "bonjour"},
      %{phrase: "こんいちは"},
    ]
    assert messages_1 == messages
    assert messages_2 == messages
  end

  test "13.1 - basic teach db some SQL" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "CREATE TABLE pets (animal, name)")
    # Prepare, cache, and register named statements. 
    Depo.teach(db, %{
      make_dog: "INSERT INTO pets VALUES('dog', 'willem')",
      make_cat: "INSERT INTO pets VALUES('cat', 'franklin')",
      get_pets: "SELECT * FROM pets",
    })
    Depo.write(db, :make_dog)
    Depo.write(db, :make_cat)
    assert Depo.read(db, :get_pets) == [
      %{animal: "cat", name: "franklin"},
      %{animal: "dog", name: "willem"}, 
    ]
  end

  test "14.1 - successful db transaction" do
    {:ok, db} = Depo.open(:memory)
    Depo.transact(db, fn ->
      Depo.write(db, "CREATE TABLE pets (animal, name);")
      Enum.each(1..5, fn i ->
        Depo.write(db, "INSERT INTO pets VALUES ('thing', 'thing #{i}')")
      end)
    end)
    pet = %{animal: "thing", name: "thing 1"}
    assert Depo.read(db, "SELECT * FROM pets LIMIT 1") == [pet]
  end

  test "14.2 - successful nested transactions" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "CREATE TABLE pets (animal, name);")
    Depo.transact(db, fn ->
      Depo.write(db, "INSERT INTO pets VALUES ('dog', 'bubbles')")
      Depo.transact(db, fn ->
        Depo.write(db, "INSERT INTO pets VALUES ('cat', 'monet')")
      end)
      Depo.write(db, "INSERT INTO pets VALUES ('fish', 'pablo')")
    end)
    assert Depo.read(db, "SELECT * FROM pets") == [
      %{animal: "fish", name: "pablo"},
      %{animal: "cat", name: "monet"},
      %{animal: "dog", name: "bubbles"},
    ]
  end

  test "26.1 - returned value format options" do
    # Set up the database.
    {:ok, db} = Depo.open(:memory)
    Depo.transact(db, fn ->
      Depo.write(db, "CREATE TABLE greetings (phrase)")
      Depo.teach(db, %{
        new_greeting: "INSERT INTO greetings VALUES (?1)",
        greetings: "SELECT * FROM greetings",
        phrases: "SELECT phrase FROM greetings",
        first_phrase: "SELECT phrase FROM greetings LIMIT 1",
      })
      Enum.each(["hola", "bonjour", "今日は"], fn phrase ->
        Depo.write(db, :new_greeting, [phrase])
      end)
    end)

    assert Depo.read(db, :greetings) == [
      %{phrase: "今日は"},
      %{phrase: "bonjour"},
      %{phrase: "hola"}, 
    ]

    # Any query can be streamed to a PID. 
    stream = Depo.stream(db, self(), :phrases)
    messages = Depo.read_stream(stream)

    assert messages == [ 
      %{phrase: "今日は"},
      %{phrase: "bonjour"},
      %{phrase: "hola"}, 
    ]

    assert Depo.read(db, :first_phrase) == [%{phrase: "hola"}]
  end

  test "24.1 - teach and stream with single bound variable" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "
    CREATE TABLE friends (
      id INTEGER PRIMARY KEY,
      name
    )")
    Depo.teach(db, %{
      new_friend: "INSERT INTO friends (name) VALUES (?1)",
      friends_named: "SELECT * FROM friends WHERE name=?1",
    })
    Depo.write(db, :new_friend, "joy")
    Depo.write(db, :new_friend, "ash")
    Depo.write(db, :new_friend, "joy")
    Depo.write(db, :new_friend, "joy")
    Depo.write(db, :new_friend, "misty")

    stream = Depo.stream(db, self(), :friends_named, "joy")
    messages = Depo.read_stream(stream) |> Enum.reverse()

    assert messages == [
      %{id: 1, name: "joy"}, 
      %{id: 3, name: "joy"}, 
      %{id: 4, name: "joy"}
    ]
  end

  test "24.2 - teach then read with single bound variable" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "
    CREATE TABLE friends (
      id INTEGER PRIMARY KEY,
      name
    )")
    Depo.teach(db, %{
      friend_id: "SELECT id FROM friends WHERE name=?1"
    })
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('joy')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('ash')")
    Depo.write(db, 
      "INSERT INTO friends (name) VALUES ('misty')")

    assert Depo.read(db, :friend_id, "misty") == [%{id: 3}]
    assert Depo.read(db, :friend_id, "joy") == [%{id: 1}]
    assert Depo.read(db, :friend_id, "ash") == [%{id: 2}]
  end

  test "28.1 - open or_create creates new db or opens existing" do
    path = Path.join(System.tmp_dir(), "depo.db")
    assert(not(File.exists?(path)), 
      "Database file shouldn't exist first.")
    {:ok, db} = Depo.open(or_create: path)
    assert :ok == Depo.close(db)
    # open existing database
    {:ok, db} = Depo.open(or_create: path)
    assert :ok == Depo.close(db)
    File.rm(path)
  end

  test "29.1 - return value from transaction function" do
    {:ok, db} = Depo.open(:memory)
    Depo.write(db, "
    CREATE TABLE friends (
      id INTEGER PRIMARY KEY,
      name
    )")
    friends = Depo.transact(db, fn ->
      Depo.write(db, 
        "INSERT INTO friends (name) VALUES ('joy')")
      Depo.write(db, 
        "INSERT INTO friends (name) VALUES ('ash')")
      Depo.read(db, "SELECT * FROM friends")
    end)

    assert friends == [
      %{id: 2, name: "ash"},
      %{id: 1, name: "joy"},
    ]
  end
end








